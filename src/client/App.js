import React from 'react';
import './app.css';
import {
  EuiPageTemplate,
  EuiEmptyPrompt,
  EuiText
} from '@elastic/eui';
import ProductPage from './Pages/Products';
import JIBELogo from './jibe-logo.svg';

export default function App() {
  const title = (
    <EuiText>
      <h1>Hi</h1>
    </EuiText>
  );
  const content = (
    <img src={JIBELogo} alt="jibe" />
  );
  return (
    <div>
      <EuiPageTemplate
        template="default"
        pageContentProps={{ paddingSize: 'none' }}
      >
        <EuiEmptyPrompt
          title={title}
          body={content}
        />
        <ProductPage />
      </EuiPageTemplate>
    </div>
  );
}

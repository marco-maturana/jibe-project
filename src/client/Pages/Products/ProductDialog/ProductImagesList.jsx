import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ImageList from '@material-ui/core/ImageList';
import ImageListItem from '@material-ui/core/ImageListItem';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper,
  },
  imageList: {
    flexWrap: 'nowrap',
    transform: 'translateZ(0)',
  },
}));

export default function ProductImagesList({ images }) {
  const classes = useStyles();

  return (
    <div className={classes.root} style={{ width: '100%' }}>
      <ImageList className={classes.imageList}>
        {images.map(item => (
          <ImageListItem key={item.url}>
            <img src={item.url} alt={item.url} />
          </ImageListItem>
        ))}
      </ImageList>
    </div>
  );
}

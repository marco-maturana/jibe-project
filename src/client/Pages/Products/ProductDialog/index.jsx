import React, { useState, useEffect } from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import useMediaQuery from '@mui/material/useMediaQuery';
import Switch from '@mui/material/Switch';
import TextField from '@mui/material/TextField';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import InputAdornment from '@mui/material/InputAdornment';
import FormControl from '@mui/material/FormControl';
import FormControlLabel from '@mui/material/FormControlLabel';
import Box from '@mui/material/Box';
import { useTheme } from '@mui/material/styles';
import Snackbar from '@mui/material/Snackbar';
import MuiAlert from '@mui/material/Alert';

import ProductImageList from './ProductImagesList';

const Alert = React.forwardRef((props, ref) => <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />);

export default function ProductDialog({
  open, mode, selectedProduct, handleClose
}) {
  const [product, setProduct] = useState(null);
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('md'));

  useEffect(() => {
    setProduct(selectedProduct);
  }, [selectedProduct]);

  const handlePriceChange = () => (event) => {
    setProduct({ ...product, price: event.target.value });
  };

  const handleInStockChange = () => (event) => {
    setProduct({ ...product, inStock: event.target.checked });
  };

  const handleSave = () => {
    const { id, price, inStock } = product;
    fetch('/api/products/update', {
      method: 'PUT',
      headers: new Headers({ 'Content-Type': 'application/json' }),
      body: JSON.stringify({
        data: { id, price, inStock }
      })
    });

    setSnackbarOpen(true);
  };

  return (
    <Dialog
      fullScreen={fullScreen}
      open={open}
      onClose={handleClose}
      aria-labelledby="responsive-dialog-title"
    >
      <DialogContent>
        <Box
          component="form"
          sx={{
            '& > :not(style)': { m: 1, width: '25ch' },
          }}
          noValidate
          autoComplete="off"
        >
          <TextField value={product?.title ?? ''} label="Title" variant="outlined" disabled />
          <FormControl>
            <InputLabel htmlFor="outlined-adornment-amount">Price</InputLabel>
            <OutlinedInput
              id="outlined-adornment-amount"
              value={product?.price ?? 0.00}
              startAdornment={<InputAdornment position="start">$</InputAdornment>}
              label="Amount"
              onChange={handlePriceChange()}
              disabled={mode === 'visualization'}
            />
          </FormControl>
          <FormControl fullWidth sx={{ m: 1 }}>
            <InputLabel htmlFor="outlined-adornment-amount">Sale Price</InputLabel>
            <OutlinedInput
              id="outlined-adornment-amount"
              value={product?.salePrice ?? 0.00}
              startAdornment={<InputAdornment position="start">$</InputAdornment>}
              label="Sale Price"
              disabled
            />
          </FormControl>
          <TextField value={product?.color ?? ''} label="Color" variant="outlined" disabled />
          <TextField value={product?.categories ?? []} label="Cetgories" variant="outlined" disabled />
          <TextField value={product?.sizes ?? []} label="Sizes" variant="outlined" disabled />
          <FormControlLabel
            value="top"
            control={(
              <Switch
                color="primary"
                checked={product?.inStock ?? false}
                onChange={handleInStockChange()}
                disabled={mode === 'visualization'}
              />
              )}
            label="In Stock?"
            labelPlacement="top"
          />
          <ProductImageList images={product?.images ?? []} />
        </Box>
      </DialogContent>
      <DialogActions>
        <Button autoFocus color="error" onClick={handleClose}>
          Close
        </Button>
        <Button disabled={mode === 'visualization'} variant="contained" onClick={handleSave} autoFocus>
          Save
        </Button>
      </DialogActions>
      <Snackbar open={snackbarOpen} autoHideDuration={6000} onClose={() => setSnackbarOpen(false)}>
        <Alert onClose={() => setSnackbarOpen(false)} severity="success" sx={{ width: '100%' }}>
          This is a success message!
        </Alert>
      </Snackbar>
    </Dialog>
  );
}

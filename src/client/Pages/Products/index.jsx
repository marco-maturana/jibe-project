import React, { useState, useEffect } from 'react';
import {
  DataGrid,
  GridActionsCellItem,
} from '@mui/x-data-grid';
import LinearProgress from '@mui/material/LinearProgress';
import EditIcon from '@mui/icons-material/Edit';
import VisibilityIcon from '@mui/icons-material/Visibility';
import Avatar from '@mui/material/Avatar';

import ProductDialog from './ProductDialog';

export default function ProductPage() {
  const [loading, setLoading] = useState(true);
  const [products, setProducts] = useState([]);
  const [selectedProduct, setSelectedProduct] = useState(null);
  const [productDialogOpen, setProductDialogOpen] = React.useState(false);
  const [productDialogMode, setProductDialogMode] = React.useState('visualization');

  const findProducts = async () => {
    try {
      const res = await fetch('/api/products/find');
      const data = await res.json();

      setLoading(false);
      setProducts(data.products);
    } catch (error) {
      throw new Error(error);
    }
  };

  const handleProductDialogOpen = (product, mode) => {
    setSelectedProduct(product);
    setProductDialogMode(mode);
    setProductDialogOpen(true);
  };

  const handleProductDialogClose = () => {
    findProducts();
    setProductDialogOpen(false);
  };

  useEffect(() => {
    findProducts();
  }, []);

  const columns = [
    {
      field: 'actions',
      type: 'actions',
      width: 100,
      getActions: args => [
        <GridActionsCellItem
          icon={<EditIcon />}
          label="Edit"
          className="textPrimary"
          onClick={() => {
            handleProductDialogOpen(args.row, 'edition');
          }}
          color="inherit"
        />,
        <GridActionsCellItem
          icon={<VisibilityIcon />}
          label="Show"
          className="textPrimary"
          onClick={() => {
            handleProductDialogOpen(args.row, 'visualization');
          }}
          color="inherit"
        />,
      ],
    },
    {
      field: 'thumbnail',
      type: 'actions',
      width: 60,
      renderCell: params => <Avatar src={params?.row?.thumbnail} />
    },
    {
      field: 'title',
      headerName: 'Title',
      flex: 4,
      minWidth: 200,
      filterable: false,
    },
    {
      field: 'categories',
      headerName: 'Categories',
      flex: 3,
      minWidth: 200,
    },
    {
      field: 'sizes',
      headerName: 'Sizes',
      flex: 2,
      minWidth: 100,
    },
    {
      field: 'color',
      headerName: 'Color',
      flex: 1,
      minWidth: 100,
    },
    {
      field: 'price',
      headerName: 'Price',
      type: 'number',
      flex: 2,
      minWidth: 100,
      filterable: false
    },
  ];

  return (
    <div style={{ height: 400, width: '100%' }}>
      <ProductDialog
        open={productDialogOpen}
        mode={productDialogMode}
        selectedProduct={selectedProduct}
        handleClose={handleProductDialogClose}
      />
      <DataGrid
        editMode="row"
        rows={products ?? []}
        columns={columns}
        pageSize={5}
        rowsPerPageOptions={[5]}
        loading={loading}
          // onRowEditStart={handleRowEditStart}
          // onRowEditStop={handleRowEditStop}
          // processRowUpdate={processRowUpdate}
        components={{
          LoadingOverlay: LinearProgress,
        }}
        experimentalFeatures={{ newEditingApi: true }}
      />
    </div>
  );
}

import { Sequelize } from 'sequelize';

const dbName = process.env.DB_NAME ?? 'jibe';
const dbUser = process.env.DB_USER ?? 'root';
const dbHost = process.env.DB_HOST ?? 'localhost';
const dbPort = process.env.DB_PORT ?? 3306;
const dbPassword = process.env.DB_PASSWORD

export default new Sequelize(dbName, dbUser, dbPassword, {
  host: dbHost,
  port: Number(dbPort),
  dialect: 'mysql',
});

import { Request, Response } from 'express';

import Product from '../model/product';

export const findAll = async (_req: Request, res: Response) => {
  const products = await Product.findAll();

  return res.json({ products });
};

export const update = async (req: Request, res: Response) => {
  const { data } = req.body;

  console.log('data', req.body);

  const product = await Product.findByPk(data.id);

  if (product == null) return res.status(404).json({ error: 'Product not found' });

  product.price = data.price;
  product.inStock = data.inStock;

  await product.save();

  return res.json({ product });
};

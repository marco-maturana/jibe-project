import {
  DataTypes, Model, InferAttributes, InferCreationAttributes, CreationOptional
} from 'sequelize';
import database from '../config/database';

class Product extends Model<InferAttributes<Product>, InferCreationAttributes<Product>> {
  declare id: CreationOptional<number>;

  declare title: string;

  declare price: number;

  declare salePrice: number;

  declare inStock: boolean;

  declare thumbnail: string;

  declare images: {
    url: string;
  }[];

  declare categories: string[];

  declare color: string;

  declare sizes: string[];
}


Product.init(
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      allowNull: false
    },
    title: {
      type: DataTypes.STRING(500),
      allowNull: false,
      unique: true,
    },
    price: {
      type: DataTypes.DECIMAL(10, 2),
      defaultValue: 0,
    },
    salePrice: {
      type: DataTypes.DECIMAL(10, 2),
      defaultValue: 0,
    },
    inStock: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
    thumbnail: {
      type: DataTypes.TEXT,
    },
    images: {
      type: DataTypes.JSON,
    },
    categories: {
      type: DataTypes.JSON
    },
    color: {
      type: DataTypes.STRING,
    },
    sizes: {
      type: DataTypes.JSON
    },
  },
  {
    tableName: 'products',
    sequelize: database,
  }
);

export default Product;

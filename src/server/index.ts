import express, { Request, Response } from 'express';
import os from 'os';
import database from './config/database';
import router from './router';
import seed from './seed';

const DEFAULT_PORT = 5424;

(async () => {
  const app = express();

  const isProductionEnv = process.env.NODE_ENV === 'production';
  const port = process.env.PORT ?? DEFAULT_PORT;

  app.use(express.json());
  app.use(express.static('dist/public'));
  app.use('/api/', router);

  app.listen(port, () => {
    console.log(`Listening on port ${port}!`);
  });

  await database.sync({ force: !isProductionEnv });

  if (!isProductionEnv && process.env.SEED_ENABLED) await seed();
})();

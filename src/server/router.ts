/* eslint-disable import/extensions */
import express from 'express';

import { findAll, update } from './controller/products';

const router = express.Router();

router.get('/products/find', async (req, res) => findAll(req, res));
router.put('/products/update', async (req, res) => update(req, res));

export default router;
